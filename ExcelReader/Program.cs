﻿using System;
using Bytescout.Spreadsheet;

namespace ExcelReader
{
    class Program
    {
        static void Main(string[] args)
        {
            //Create new spreadsheet
            Spreadsheet document = new Spreadsheet();
            document.LoadFromFile("sample.xlsx");

            // Ger worksheet by name
            Worksheet worksheet = document.Workbook.Worksheets.ByName("Sheet1");
           
            //Check Dates
            for (int i = 0; i < 3; i++)
            {
                // Set current cell
                Cell currentCell = worksheet.Cell(i, 0);

                DateTime date = currentCell.ValueAsDateTime;

                // Write Date
                Console.WriteLine("{0}", date.ToShortDateString());
            }

            //Close document
            document.Close();
          
        }
    }
}
